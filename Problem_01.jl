### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 44b2a940-c90d-11ec-2f15-87f5cb2eaccc
using Markdown

# ╔═╡ f29481d8-80de-4c92-8ff1-d88b3e2f889f
using InteractiveUtils

# ╔═╡ 34b7bd52-3ca5-40b6-b1ab-01bf17621266
using PlutoUI

# ╔═╡ 53694bac-1db2-4a90-988c-e138d6c3f28a
using DataStructures

# ╔═╡ 4c7d758a-7378-4b52-a0e1-187213ae76dd
md"## State creation "

# ╔═╡ ab807525-f213-450c-8823-979dec6df4d3
struct State
    name::String
    position::Int64
   parcels::Vector{Bool}
end

# ╔═╡ 5f4d2062-3a7f-447e-9050-df0882291087
md"## Action creation "

# ╔═╡ 24a3398c-1c55-482f-addb-fd0bd0dc0b16
struct Action
    name::String
    cost::Int64
end

# ╔═╡ 28aec757-8e24-4fac-8214-43590313cb16
md"## Assigning actions "

# ╔═╡ 46f9aea7-58df-4980-8bf3-c9f13a59e00c
Action1 = Action("me", 2)

# ╔═╡ ad785c15-3d93-4dd7-a3f0-d4256ac54a72
Action2 = Action("mw", 2)

# ╔═╡ d35778dc-be02-46e0-b5a5-fbdd782c4d17
Action3 = Action("mu", 1)

# ╔═╡ 3fb6fdd0-3de1-4e5a-8d01-246dbc53d3dd
Action4 = Action("md", 1)

# ╔═╡ fb44310b-4042-4959-ab8f-98bd396d5a4c
Action5 = Action("co", 5)

# ╔═╡ 07983962-6274-4e1d-b5ce-0911aa51f15e
md"## Assigning the states  "

# ╔═╡ e3fd8b7b-f543-4e29-b288-9b12c696ac9a
State1 = State("State one", 1, [true, false, true])

# ╔═╡ 7cd5e47d-5493-43fd-97bc-f67c7357af23
State2 = State("State two", 2, [true, true, false])

# ╔═╡ 7dd02ec9-dd8e-44ee-97f7-baa03fbe1805
State3 = State("State three", 3, [false, true, true])

# ╔═╡ 68cc22cd-605e-4a54-9787-54e2b425277a
State4 = State("State four", 4, [false, true, true])

# ╔═╡ 2cc8d6af-19d5-4124-9ae6-42e250abf573
State5 = State("State five", 5, [true, false, true])

# ╔═╡ 1efd8cbc-b7ef-4ad0-9738-7f4b5179dcb6
State6 = State("State six", 1, [false, true, true])

# ╔═╡ f478bd39-f9a6-4ac5-9b3e-75e978059ed1
State7 = State("State seven", 2, [false, false, true])

# ╔═╡ 8cc0a1eb-d42f-4e31-b36a-896b9d85bcdd
State8 = State("State eight", 3, [false, false,true])

# ╔═╡ 7e5b25df-6e46-4ad1-ad24-50c0cc083f1a
State9 = State("State nine", 4, [false, false, true])

# ╔═╡ f2abac48-9153-4e74-a802-2c6f291587e3
State10 = State("State ten", 5, [false, false, true])

# ╔═╡ 8194a085-bf49-4753-9790-b270b040fc6b
State11 = State("State eleven", 5, [false, false, false])

# ╔═╡ 037d7052-b9b1-458f-8da5-c40adfa3ff1a
md"## Transition Model"

# ╔═╡ 504d5a02-028d-46ec-abde-6f83ad35e7d0
TransitionModel = Dict()

# ╔═╡ 98d27f44-4281-4557-96cd-e23334ac2c4c
md"### Add a mapping to the transition Model"

# ╔═╡ 0ed55a8d-b1a5-4e39-b37f-c08402c47b41
push!(TransitionModel, State1 => [(Action2, State2), (Action5, State6), (Action3, State1)])

# ╔═╡ 5d857a5e-4b79-44d0-a618-378d79be1512
push!(TransitionModel, State1 => [(Action2, State3), (Action5, State7), (Action1, State2)])

# ╔═╡ 0841ff83-56a9-4912-823e-a62beb481a9f
push!(TransitionModel, State1 => [(Action4, State4), (Action5, State8), (Action1, State3)])

# ╔═╡ 71ce30aa-13a8-412f-b6df-4338cc4c3ead
push!(TransitionModel, State1 => [(Action2, State5), (Action5, State9), (Action3, State4)])

# ╔═╡ 3184102f-81e4-486d-b951-9095ac26ac9e
push!(TransitionModel, State10 => [(Action2, State5), (Action5, State10), (Action4, State11)])

# ╔═╡ 0b5b74bf-fee6-4093-89c0-955af56917ce
TransitionModel

# ╔═╡ 2df76104-facc-48aa-ae12-5f0d67958b13
md"### A* Search strategy"

# ╔═╡ 9910fa1a-2b79-4fd2-9d3e-6f06575fad74
function heuristic(cell, goalState)
    return distance(cell[1] - goalState[1]) + distance(cell[2] - goalState[2])
end

# ╔═╡ a7e49bfb-ef6e-46b6-88fc-bd5b40748d05
function AstarSearch(TransitionModel,initialState, goalState)
	
	# setting result queue to empty list
    result = []
	frontier = Queue{State}()
	explored = []
	parents = Dict{State, State}()
	first_state = true
    enqueue!(frontier, initialState)
	parent = initialState
	# remove last item from result queue if queue is not empty
    while true
		if isempty(frontier)
			return []
		else
			currentState = dequeue!(frontier)
			push!(explored, currentState)
			candidates = TransitionModel[currentState]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(parents, single_candidate[2] => currentState)
					if (single_candiadte[2] in goal_state)
						return get_result(TransitionModel, parents,initialState, single_candidate[2])
					else
						enqueue!(frontier, single_candidate[2])
					end
				end
			end
		end
	end
end

# ╔═╡ f210935c-2fca-4db1-869f-cdf8be040d80
function goal_test(currentState::State)
    return ! (currentState.parcels[1] || currentState.parcel[2])
end

# ╔═╡ 24ef637a-27bf-4d8d-9c7b-de000b36db75
function add_to_queue(queue::Queue{State}, state::State, cost::Int64)
    enqueue!(queue, state)
    return queue
end

# ╔═╡ cee35595-f4d3-4b19-a75f-a515f70659e0
function add_to_stack(stack::Stack{State}, state::State, cost::Int64)
    push!(stack, state)
    return stack
end

# ╔═╡ b5fe3792-b622-47d2-bb72-f63599735e3d
function remove_from_queue(queue::Queue{State})
    removed = dequeue!(queue)
    return (removed, queue)
end

# ╔═╡ abaeadf7-7952-4097-9b94-e7bd8959f5ec
function remove_from_stack(stack::Stack{State})
    removed = pop!(stack)
    return (removed, stack)
end

# ╔═╡ 9ac1d2b6-da7c-4cf3-8fb7-949329bd06e0
function add_pqueue_ucs(queue::PriorityQueue{State, Int64}, state::State,
cost::Int64)
    enqueue!(queue, state, cost)
    return queue
end

# ╔═╡ 84ba07a7-110f-468f-9d5e-ab29da948889
function remove_from_pqueue_ucs(queue::PriorityQueue{State, Int64})
    removed = dequeue!(queue)
    return (removed, queue)
end

# ╔═╡ cac0d1d1-efc4-4391-81b1-96c7476d66f1
function input()
    
end

# ╔═╡ ef5be6b9-95f9-430e-b79d-14af3c965eca
md"## User Input"

# ╔═╡ 78848dd1-3614-4aab-bc4f-1233a1a297d4
@bind storeys TextField()

# ╔═╡ effd4b9c-d09b-4950-9edc-c647d0c5f428
with_terminal() do
	println("Number of storeys are:", storeys)
end

# ╔═╡ 1c71c687-6f29-4899-ba0b-0df4e082e78d
@bind offices TextField()

# ╔═╡ 7c9c4f7a-7f24-46fe-aff9-d5ce0c9eb040
with_terminal() do
	println("Number of occupied offices per floor are:", offices)
end

# ╔═╡ 2567b992-3040-4dc3-8b08-1211de16ea1e
@bind parcels TextField()

# ╔═╡ a227662c-3647-4043-a2b8-e746b19632f8
with_terminal() do
	println("Number of parcels in each office are:", parcels)
end

# ╔═╡ b8f263d2-a1f4-4085-9261-5fe928e534a6
@bind location Radio(["State1", "State2", "State3"])

# ╔═╡ 34ca3f37-789f-4de8-98e8-441c1ae833b5
with_terminal() do
	println("Location of the agent at the beginning is:", location)
end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
InteractiveUtils = "b77e0a4c-d291-57a0-90e8-8db25a27a240"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═44b2a940-c90d-11ec-2f15-87f5cb2eaccc
# ╠═f29481d8-80de-4c92-8ff1-d88b3e2f889f
# ╠═34b7bd52-3ca5-40b6-b1ab-01bf17621266
# ╠═53694bac-1db2-4a90-988c-e138d6c3f28a
# ╠═4c7d758a-7378-4b52-a0e1-187213ae76dd
# ╠═ab807525-f213-450c-8823-979dec6df4d3
# ╠═5f4d2062-3a7f-447e-9050-df0882291087
# ╠═24a3398c-1c55-482f-addb-fd0bd0dc0b16
# ╠═28aec757-8e24-4fac-8214-43590313cb16
# ╠═46f9aea7-58df-4980-8bf3-c9f13a59e00c
# ╠═ad785c15-3d93-4dd7-a3f0-d4256ac54a72
# ╠═d35778dc-be02-46e0-b5a5-fbdd782c4d17
# ╠═3fb6fdd0-3de1-4e5a-8d01-246dbc53d3dd
# ╠═fb44310b-4042-4959-ab8f-98bd396d5a4c
# ╠═07983962-6274-4e1d-b5ce-0911aa51f15e
# ╠═e3fd8b7b-f543-4e29-b288-9b12c696ac9a
# ╠═7cd5e47d-5493-43fd-97bc-f67c7357af23
# ╠═7dd02ec9-dd8e-44ee-97f7-baa03fbe1805
# ╠═68cc22cd-605e-4a54-9787-54e2b425277a
# ╠═2cc8d6af-19d5-4124-9ae6-42e250abf573
# ╠═1efd8cbc-b7ef-4ad0-9738-7f4b5179dcb6
# ╠═f478bd39-f9a6-4ac5-9b3e-75e978059ed1
# ╠═8cc0a1eb-d42f-4e31-b36a-896b9d85bcdd
# ╠═7e5b25df-6e46-4ad1-ad24-50c0cc083f1a
# ╠═f2abac48-9153-4e74-a802-2c6f291587e3
# ╠═8194a085-bf49-4753-9790-b270b040fc6b
# ╠═037d7052-b9b1-458f-8da5-c40adfa3ff1a
# ╠═504d5a02-028d-46ec-abde-6f83ad35e7d0
# ╠═98d27f44-4281-4557-96cd-e23334ac2c4c
# ╠═0ed55a8d-b1a5-4e39-b37f-c08402c47b41
# ╠═5d857a5e-4b79-44d0-a618-378d79be1512
# ╠═0841ff83-56a9-4912-823e-a62beb481a9f
# ╠═71ce30aa-13a8-412f-b6df-4338cc4c3ead
# ╠═3184102f-81e4-486d-b951-9095ac26ac9e
# ╠═0b5b74bf-fee6-4093-89c0-955af56917ce
# ╠═2df76104-facc-48aa-ae12-5f0d67958b13
# ╠═9910fa1a-2b79-4fd2-9d3e-6f06575fad74
# ╠═a7e49bfb-ef6e-46b6-88fc-bd5b40748d05
# ╠═f210935c-2fca-4db1-869f-cdf8be040d80
# ╠═24ef637a-27bf-4d8d-9c7b-de000b36db75
# ╠═cee35595-f4d3-4b19-a75f-a515f70659e0
# ╠═b5fe3792-b622-47d2-bb72-f63599735e3d
# ╠═abaeadf7-7952-4097-9b94-e7bd8959f5ec
# ╠═9ac1d2b6-da7c-4cf3-8fb7-949329bd06e0
# ╠═84ba07a7-110f-468f-9d5e-ab29da948889
# ╠═cac0d1d1-efc4-4391-81b1-96c7476d66f1
# ╠═ef5be6b9-95f9-430e-b79d-14af3c965eca
# ╠═78848dd1-3614-4aab-bc4f-1233a1a297d4
# ╠═effd4b9c-d09b-4950-9edc-c647d0c5f428
# ╠═1c71c687-6f29-4899-ba0b-0df4e082e78d
# ╠═7c9c4f7a-7f24-46fe-aff9-d5ce0c9eb040
# ╠═2567b992-3040-4dc3-8b08-1211de16ea1e
# ╠═a227662c-3647-4043-a2b8-e746b19632f8
# ╠═b8f263d2-a1f4-4085-9261-5fe928e534a6
# ╠═34ca3f37-789f-4de8-98e8-441c1ae833b5
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002

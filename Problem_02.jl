### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ 414585bd-aaa5-4dac-b3a3-df0ead5fae1f
using PlutoUI

# ╔═╡ 20d7bf9e-f026-4b19-a411-20817d756439
using DataStructures

# ╔═╡ bb5a298b-58b0-44d5-85cc-6eeafbf3a643
using Markdown

# ╔═╡ aaf31188-a69d-4bb1-8d3f-e67a88bf3fc6
using InteractiveUtils

# ╔═╡ 2259370e-ae94-4ba3-907d-c9d5b66d195a
using Printf

# ╔═╡ 0fa8c8eb-748d-45f7-8def-d564a91419d7
@enum Domain green red blue yellow

# ╔═╡ 0166a827-c32e-42ed-967f-6c3802b9b455
abstract type CSP
end

# ╔═╡ 747a39b3-0fcb-4c65-9b5b-119681839929
struct ConstantFunctionDict{V}
    value::V
end

# ╔═╡ cc85c121-ac9b-4abf-bdb0-2b930549b592
mutable struct CSPDict
    dict::Union{Nothing, Dict, ConstantFunctionDict}
end

# ╔═╡ 26ef8a79-b376-4cc5-aab6-19a3f6d66eea
mutable struct C_S_P <: CSP
	vars::AbstractVector
	domains::CSPDict
	neighbors::CSPDict
	constraints::Function
	initial::Tuple
	current_domains::Union{Nothing, Dict}
	nassigns::Int64
end

# ╔═╡ 5de4df91-0499-4b80-b4d2-07de3528467f
mutable struct CSPVar
	name::String
	value::Union{Nothing,Domain}
	forbidden_values::Vector{Domain}
	domain_restriction_count::Int64
end

# ╔═╡ 7796a56c-dce7-400c-a052-77b48e2814f8
struct TheCSP
	vars::Vector{CSPVar}
	constraints::Vector{Tuple{CSPVar,CSPVar}}
end

# ╔═╡ 3f24b40e-e63a-47fc-af4c-f81d9a91f127
struct graph{T <: Real,U}
    edges::Dict{Tuple{U,U},T}
    verts::Set{U}
end

# ╔═╡ e5618e0e-701e-4060-af18-d54bd79ac561
function Constraint(vars::AbstractVector, domains::CSPDict, neighbors::CSPDict, constraints::Function;
                initial::Tuple=(), current_domains::Union{Nothing, Dict}=nothing, nassigns::Int64=0)
        return new(vars, domains, neighbors, constraints, initial, current_domains, nassigns)
    end

# ╔═╡ 955f699b-ead4-4533-9100-ea12dd29b3a8
function theGraph(edges::Vector{Tuple{U,U,T}}) where {T <: Real,U}
    vnames = Set{U}(v for edge in edges for v in edge[1:2])
    adjmat = Dict((edge[1], edge[2]) => edge[3] for edge in edges)
    return graph(adjmat, vnames)
end

# ╔═╡ e0a4865a-2e2e-4169-8b85-a083886a0794
begin
	vertices(g::graph) = g.verts
	edges(g::graph)    = g.edges
end

# ╔═╡ b4d19122-cb1e-43b0-b985-79e4fe0f7c9d
con = rand(setdiff(Set([green,blue,yellow,red]), Set([red,green])))

# ╔═╡ c1160bad-f829-4652-a2ea-ea892999d988
neighbours(g::graph, v) = Set((b, c) for ((a, b), c) in edges(g) if a == v)

# ╔═╡ 7566a72b-fb6d-47c0-8544-52ce30c2b98d
function assignTo(problem::T, key, val, assignment::Dict) where {T <: CSP}
    assignment[key] = val;
    problem.nassigns = problem.nassigns + 1;
    nothing;
end

# ╔═╡ 116eeb97-fbfc-4817-b8c0-4d47e02c4704
function unassignFrom(problem::T, key, assignment::Dict) where {T <: CSP}
    if (haskey(assignment, key))
        delete!(assignment, key);
    end
    nothing;
end

# ╔═╡ df1949c7-2c97-4ecd-b23c-a46a0f33302d
function nconflicts(problem::T, key, val, assignment::Dict) where {T <: CSP}
    return count(
                (function(second_key)
                    return (haskey(assignment, second_key) &&
                        !(problem.constraints(key, val, second_key, assignment[second_key])));
                end),
                problem.neighbors[key]);
end

# ╔═╡ a2db4034-77dc-481d-844c-632a7a4a315d
function display(problem::T, assignment::Dict) where {T <: CSP}
    println("Constr: ", problem, " with assignment: ", assignment);
    nothing;
end

# ╔═╡ af9678d9-bdd2-4a41-8ab8-b99ef699b6e3
function actions(problem::T, state::Tuple) where {T <: CSP}
    if (length(state) == length(problem.vars))
        return [];
    else
        let
            local assignment = Dict(state);
            local var = problem.vars[findfirst((function(e)
                                        return !haskey(assignment, e);
                                    end), problem.vars)];
            return collect((var, val) for val in problem.domains[var]
                            if nconflicts(problem, var, val, assignment) == 0);
        end
    end
end

# ╔═╡ 9326e8ee-2552-4705-a9d6-3ad84171be05
function result(problem::T, state::Tuple, action::Tuple) where {T <: CSP}
    return (state..., action);
end

# ╔═╡ 4fa31dc3-c263-4e01-9f07-33585872070b
function searchpath(g::graph{T,U}, source::U, dest::U) where {T, U}
    @assert source ∈ vertices(g) "$source is not a vertex in the graph"
 
    if source == dest return [source], 0 end
    # Initialize variables
    inf  = typemax(T)
    dist = Dict(v => inf for v in vertices(g))
    prev = Dict(v => v   for v in vertices(g))
    dist[source] = 0
    Q = copy(vertices(g))
    neigh = Dict(v => neighbours(g, v) for v in vertices(g))
 
    # Main loop
    while !isempty(Q)
        u = reduce((x, y) -> dist[x] < dist[y] ? x : y, Q)
        pop!(Q, u)
        if dist[u] == inf || u == dest break end
        for (v, cost) in neigh[u]
            alt = dist[u] + cost
            if alt < dist[v]
                dist[v] = alt
                prev[v] = u
            end
        end
    end
 
    # Return path
    rst, cost = U[], dist[dest]
    if prev[dest] == dest
        return rst, cost
    else
        while dest != source
            pushfirst!(rst, dest)
            dest = prev[dest]
        end
        pushfirst!(rst, dest)
        return rst, cost
    end
end

# ╔═╡ 6b8966e4-1cbc-4447-b40a-d8567d97ddbb
function goal_test(problem::T, state::Tuple) where {T <: CSP}
    let
        local assignment = Dict(state);
        return (length(assignment) == length(problem.vars) &&
                all((function(key)
                            return nconflicts(problem, key, assignment[key], assignment) == 0;
                        end)
                        ,
                        problem.vars));
    end
end

# ╔═╡ a9fdc375-8b9b-4599-a37f-9d860f152b7c
function goal_test(problem::T, state::Dict) where {T <: CSP}
    let
        local assignment = deepcopy(state);
        return (length(assignment) == length(problem.vars) &&
                all((function(key)
                            return nconflicts(problem, key, assignment[key], assignment) == 0;
                        end),
                        problem.vars));
    end
end

# ╔═╡ c3520588-bbe8-45af-9b3c-c92bfc62e5b8
function path_cost(problem::T, cost::Float64, state1::Tuple, action::Tuple, state2::Tuple) where {T <: CSP}
    return cost + 1;
end

# ╔═╡ ef9bb290-1dd5-49c2-b984-6fc9fcae304e
function pruning(problem::T) where {T <:CSP}
    if (problem.current_domains === nothing)
        problem.current_domains = Dict(collect(Pair(key, collect(problem.domains[key])) for key in problem.vars));
    end
    nothing;
end

# ╔═╡ f72045ca-ba5c-475a-b5e5-dcca39e28a8f
function suppose(problem::T, key, val) where {T <: CSP}
    pruning(problem);
    local removals::AbstractVector = collect(Pair(key, a) for a in problem.current_domains[key]
                                            if (a != val));
    problem.current_domains[key] = [val];
    return removals;
end

# ╔═╡ c8ab549d-aecc-4988-b692-a0dbfe3b2ab2
function prune(problem::T, key, value, removals) where {T <: CSP}
    local not_removed::Bool = true;
    for (i, element) in enumerate(problem.current_domains[key])
        if (element == value)
            deleteat!(problem.current_domains[key], i);
            not_removed = false;
            break;
        end
    end
    if (not_removed)
        error("Could not find ", value, " in ", problem.current_domains[key], " for key '", key, "' to be removed!");
    end
    if (!(typeof(removals) <: Nothing))
        push!(removals, Pair(key, value));
    end
    nothing;
end

# ╔═╡ a46b0048-9cce-4590-af45-b45798f26a58
function check(problem::T, key) where {T <: CSP}
    if (!(problem.current_domains === nothing))
        return problem.current_domains[key];
    else
        return problem.domains[key];
    end
end

# ╔═╡ 3e43fedf-7815-4632-8404-bfabb9531180
function infer_assignment(problem::T) where {T <: CSP}
    pruning(problem);
    return Dict(collect(Pair(key, problem.current_domains[key][1])
                        for key in problem.vars
                            if (1 == length(problem.current_domains[key]))));
end

# ╔═╡ 99a98cad-ac58-4166-af92-06e27159befc
function restore(problem::T, removals::AbstractVector) where {T <: CSP}
    for (key, val) in removals
        push!(problem.current_domains[key], val);
    end
    nothing;
end

# ╔═╡ 4c4b93ec-709a-49ad-a796-cb7569ed317d
function conflicted_variables(problem::T, current_assignment::Dict) where {T <: CSP}
    return collect(var for var in problem.vars
                    if (nconflicts(problem, var, current_assignment[var], current_assignment) > 0));
end

# ╔═╡ d870afcc-6056-463e-a1ed-b2cad78d7090
function forward_checking(problem::T, var, value, assignment::Dict, removals::Union{Nothing, AbstractVector}) where {T <: CSP}
    for B in problem.neighbors[var]
        if (!haskey(assignment, B))
            for b in copy(problem.current_domains[B])
                if (!problem.constraints(var, value, B, b))
                    prune(problem, B, b, removals);
                end
            end
            if (length(problem.current_domains[B]) == 0)
                return false;
            end
        end
    end
    return true;
end

# ╔═╡ 74b15c27-96aa-4054-ae6f-7d3aef0f7fc3
function backtrack(problem::T, assignment::Dict;
                    select_unassigned_variable::Function=first_unassigned_variable,
                    order_domain_values::Function=unordered_domain_values,
                    inference::Function=no_inference) where {T <: CSP}
    if (length(assignment) == length(problem.vars))
        return assignment;
    end
    local var = select_unassigned_variable(problem, assignment);
    for value in order_domain_values(problem, var, assignment)
        if (nconflicts(problem, var, value, assignment) == 0)
            assign(problem, var, value, assignment);
            removals = suppose(problem, var, value);
            if (inference(problem, var, value, assignment, removals))
                result = backtrack(problem, assignment,
                                    select_unassigned_variable=select_unassigned_variable,
                                    order_domain_values=order_domain_values,
                                    inference=inference);
                if (!(typeof(result) <: Nothing))
                    return result;
                end
            end
            restore(problem, removals);
        end
    end
    unassign(problem, var, assignment);
    return nothing;
end

# ╔═╡ 28b5e356-840e-444b-9420-12fa97361bb2
testgraph = [("a", "b", 1), ("b", "e", 2), ("a", "e", 4)]

# ╔═╡ f03eed6b-5407-4d6a-bbee-a93f11021a94
g = theGraph(testgraph)

# ╔═╡ 6854b671-0197-44be-a4c8-0d06ada5c8cd
src, dst = "a", "e"

# ╔═╡ ab04096c-41ec-49d8-9525-da01e2f7c6c0
path, cost = searchpath(g, src, dst)

# ╔═╡ 3d7944c5-a8de-49d1-8168-ac9a02ee5133
with_terminal() do
	println("Shortest path from $src to $dst: ", isempty(path) ? "no possible path" : join(path, " → "), " (cost $cost)")
end

# ╔═╡ 28515dfc-18a9-4d2e-9ad2-65b7cedc603d
with_terminal() do
	@printf("\n%4s | %3s | %s\n", "src", "dst", "path")
    @printf("-----------------\n")
	for src in vertices(g), dst in vertices(g)
    path, cost = searchpath(g, src, dst)
    @printf("%4s | %3s | %s\n", src, dst, isempty(path) ? "no possible path" : join(path, " → ") * " ($cost)")
	end
end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
InteractiveUtils = "b77e0a4c-d291-57a0-90e8-8db25a27a240"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
Printf = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═414585bd-aaa5-4dac-b3a3-df0ead5fae1f
# ╠═20d7bf9e-f026-4b19-a411-20817d756439
# ╠═bb5a298b-58b0-44d5-85cc-6eeafbf3a643
# ╠═aaf31188-a69d-4bb1-8d3f-e67a88bf3fc6
# ╠═2259370e-ae94-4ba3-907d-c9d5b66d195a
# ╠═0fa8c8eb-748d-45f7-8def-d564a91419d7
# ╠═0166a827-c32e-42ed-967f-6c3802b9b455
# ╠═747a39b3-0fcb-4c65-9b5b-119681839929
# ╠═cc85c121-ac9b-4abf-bdb0-2b930549b592
# ╠═26ef8a79-b376-4cc5-aab6-19a3f6d66eea
# ╠═5de4df91-0499-4b80-b4d2-07de3528467f
# ╠═7796a56c-dce7-400c-a052-77b48e2814f8
# ╠═3f24b40e-e63a-47fc-af4c-f81d9a91f127
# ╠═e5618e0e-701e-4060-af18-d54bd79ac561
# ╠═955f699b-ead4-4533-9100-ea12dd29b3a8
# ╠═e0a4865a-2e2e-4169-8b85-a083886a0794
# ╠═b4d19122-cb1e-43b0-b985-79e4fe0f7c9d
# ╠═c1160bad-f829-4652-a2ea-ea892999d988
# ╠═7566a72b-fb6d-47c0-8544-52ce30c2b98d
# ╠═116eeb97-fbfc-4817-b8c0-4d47e02c4704
# ╠═df1949c7-2c97-4ecd-b23c-a46a0f33302d
# ╠═a2db4034-77dc-481d-844c-632a7a4a315d
# ╠═af9678d9-bdd2-4a41-8ab8-b99ef699b6e3
# ╠═9326e8ee-2552-4705-a9d6-3ad84171be05
# ╠═4fa31dc3-c263-4e01-9f07-33585872070b
# ╠═6b8966e4-1cbc-4447-b40a-d8567d97ddbb
# ╠═a9fdc375-8b9b-4599-a37f-9d860f152b7c
# ╠═c3520588-bbe8-45af-9b3c-c92bfc62e5b8
# ╠═ef9bb290-1dd5-49c2-b984-6fc9fcae304e
# ╠═f72045ca-ba5c-475a-b5e5-dcca39e28a8f
# ╠═c8ab549d-aecc-4988-b692-a0dbfe3b2ab2
# ╠═a46b0048-9cce-4590-af45-b45798f26a58
# ╠═3e43fedf-7815-4632-8404-bfabb9531180
# ╠═99a98cad-ac58-4166-af92-06e27159befc
# ╠═4c4b93ec-709a-49ad-a796-cb7569ed317d
# ╠═d870afcc-6056-463e-a1ed-b2cad78d7090
# ╠═74b15c27-96aa-4054-ae6f-7d3aef0f7fc3
# ╠═28b5e356-840e-444b-9420-12fa97361bb2
# ╠═f03eed6b-5407-4d6a-bbee-a93f11021a94
# ╠═6854b671-0197-44be-a4c8-0d06ada5c8cd
# ╠═ab04096c-41ec-49d8-9525-da01e2f7c6c0
# ╠═3d7944c5-a8de-49d1-8168-ac9a02ee5133
# ╠═28515dfc-18a9-4d2e-9ad2-65b7cedc603d
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002

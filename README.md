# ARI711S Assignment 02 - 214078485 

## Author
Full Name:  Salem Tangeni Hailonga<br>
Student No. 214078485<br>
Mode of Study: Part-Time<br>

## Introduction

After careful reading and grasping the assignment concept, It is required of me to give a solution to the below problems stipulated in the assignment using Julia programming language and use the Flux package for neural networks.

### Problem 1.
Consider a multi-storey building that serves as the office for Company X. Consider an agent in charge of the logistics in the company. Once a week, the agent collects parcels from all offices in the building and sends them to their destination. Each floor has a number of contiguous offices, each one with a specific room number. 

The possible actions the agent can execute are defined as follows:
move east: (me), to move to the next office eastward;
move west: (mw), to move to the next office westward;
move up: (mu), to move to the floor up;
move down: (md), to move to the floor down;
collect: (co), to collect a single item from an office.

Additionally, note that the agent can only collect one item at a time and can do so only if it is in the same office where the item is situated. Furthermore, moving eastward (or westward) is possible if the destination office exists. In the same vein, moving up or down is possible only if the destination floor exists. Finally, the cost attached to each action is defined in Table 1. 

Your task is to write a program in Julia that implements a search-based problem-solving mechanism using the A ∗ algorithm as a strategy. As a
heuristic function, you will apply 3 unit costs for each parcel not yet collected. A goal state is one where all parcels have been collected.
Your program will allow the user to provide a detailed configuration of the problem at runtime. This includes the number of storeys, how many occupied offices per floor, the number of parcels in each office and the location of the agent at the beginning.

### Problem 2.
In this problem, we wish to solve a function selection problem using constraint satisfaction techniques. Consider a composition of actions represented by a constraint graph and a collection of serverless functions from cloud providers. Each action has a signature: input and output types. Similarly, each serverless function has a signature. Note that the types in the actions could be abstract, while the types in the serverless functions are concrete, including user-defined types. 
An arc in the constraint graph should be understood as a constraint between the related actions. This constraint implies type compatibility between the functions matching those actions. For example, given a constraint between two actions a0 and a1, if a serverless function f has been assigned to a0, a function g can only be assigned to a1 if its signature is compatible with f.

Furthermore, each action has the following unary 1 constraints:
• a minimum response time;
• a maximum latency;
• a minimum throughput; and
• a minimum success rate.

Your task is to:
1. Write a program in Julia that allows a user to define a constraint satisfaction problem based on the previous description;
2. implement a solver in your program using forward checking and propagation;
3. extend your program to minimise the cost of your solution, given the cost attached to each serverless function.
